#ifndef _VIEW_SIZES_H__
#define _VIEW_SIZES_H__

//Screen window boundaries, two player
#define T_LEFT_ONE 0
#define T_UP_ONE 0

#define T_LEFT_TWO ((SCREEN_WIDTH) / 2 + 1)
#define T_UP_TWO 0

#define T_LEFT_THREE ((SCREEN_WIDTH) / 2 + 1)
#define T_LEFT_THREE_FOUR 0
#define T_UP_THREE ((SCREEN_HEIGHT) / 2 + 1)

#define T_LEFT_FOUR ((SCREEN_WIDTH) / 2 + 1)
#define T_UP_FOUR ((SCREEN_HEIGHT) / 2 + 1)

#define T_WIDTH (SCREEN_WIDTH)
#define T_HEIGHT (SCREEN_HEIGHT)
#define T_HALF_WIDTH (SCREEN_WIDTH / 2 - 1)
#define T_HALF_HEIGHT (SCREEN_HEIGHT / 2 - 1)

#endif
